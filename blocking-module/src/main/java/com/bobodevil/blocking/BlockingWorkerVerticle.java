package com.bobodevil.blocking;


import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.platform.Verticle;



public class BlockingWorkerVerticle extends Verticle {

	public void start() {
		EventBus eb = vertx.eventBus();
		
		new BlockingWorkerHandler(vertx, container);
		
		
		container.logger().info("Blocking Handler Started");
	}
}
