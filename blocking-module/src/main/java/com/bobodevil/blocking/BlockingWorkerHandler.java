package com.bobodevil.blocking;

import java.util.Map;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;


public class BlockingWorkerHandler {
	
	private Vertx vertx;
	private Container container;
	
	public BlockingWorkerHandler(Vertx vertx, Container container) {
		this.container = container;
		this.vertx = vertx;
		vertx.eventBus().registerHandler("com.bobodevil.blockingsleep", blockingSleepHandler());
	}
	
	public Handler<Message<JsonObject>> blockingSleepHandler() {
		return new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				container.logger().info("-- BLOCKING Start thread id : " + Thread.currentThread().getId());
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				container.logger().info("-- BLOCKING Done thread id  : " + Thread.currentThread().getId());
				message.reply(message.body());
			}
		};
	}
}
