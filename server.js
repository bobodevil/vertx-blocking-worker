var vertx = require('vertx');
var container = require('vertx/container');
var console = require('vertx/console');
var eb = vertx.eventBus;


container.deployModule("com.bobodevil~blocking-module~0.1");

vertx.createHttpServer().requestHandler(function(request) {
	console.log('request came in  : ');
	eb.send('com.bobodevil.blockingsleep', {'test':'This is a message'}, function(reply) {
		var myBuffer = 'I received a reply ' + reply;
		request.response.end(myBuffer);
	});
}).listen(11080);
  
  
  